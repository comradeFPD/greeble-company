@extends('layouts.app')
@section('title', 'Заказать и купить межкомнатные двери на Мои-Двери.рф')
@section('content')
    <div class="container">
        <div class="col-md-12">
            <h2 class='text-center'>Выберите тип двери:</h2>
            <div class="row" style='margin-top:50px;'>
                @foreach($categories as $category)
                    <div class="col-md-3 col-sm-6 col-xs-12" style='margin-top:10px;'>
                        <a href="{{ url('/products/'.$category->id) }}" target='_blank' ><img src="{{ asset('/storage/'.$category->image) }}" style='max-width:100%;'></a>
                        <a href="{{ url('/products/'.$category->id) }}" target='_blank' style='color:#222;text-decoration:none;'><h4 style='margin:15px 0;padding:4px;'>{{ $category->title }}</h4></a>
                        <a href="{{ url('/products/'.$category->id) }}" class="btn btn-success btn-block" target='_blank'><i class="fa fa-chevron-right" aria-hidden="true"></i> Выбрать</a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
