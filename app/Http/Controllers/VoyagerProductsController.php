<?php

namespace App\Http\Controllers;

use App\DoorColor;
use App\Helpers\Helper;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class VoyagerProductsController extends VoyagerBaseController
{
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        }
        else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        if ($request->hasFile('color-file-0')) {
            for ($i = 0; $i <= $request->color_count; $i++) {
                $url = Helper::saveFile($request->file('color-file-' . $i), '/products');
                $data->colors()->create([
                    'image' => $url,
                    'color_id' => $request->input('color-'.$i)
                ]);
            }
        }

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', $model)) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        }
        else {
            $redirect = redirect()->back();
        }

        return $redirect->with(
            [
                'message'    => __(
                        'voyager::generic.successfully_updated'
                    ) . " {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]
        );
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val  = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            }
            else {
                $redirect = redirect()->back();
            }
            if ($request->hasFile('color-file-0')) {
                for ($i = 0; $i <= $request->color_count; $i++) {
                    //dd($request->file('color-file-'.$i));
                    $url = Helper::saveFile($request->file('color-file-'.$i), '/products');
                    $data->colors()->create([
                        'image' => $url,
                        'color_id' => $request->input('color-'.$i)
                    ]);
                }
            }

            return $redirect->with(
                [
                    'message'    => __(
                            'voyager::generic.successfully_added_new'
                        ) . " {$dataType->getTranslatedAttribute('display_name_singular')}",
                    'alert-type' => 'success',
                ]
            );
        }
        else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function getDeleteColor($id)
    {
        DoorColor::find($id)->delete();
    }
}
