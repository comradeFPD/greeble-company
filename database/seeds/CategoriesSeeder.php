<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Распашные', 'Раздвижные', "Складные", "Поворотные"];
        foreach ($data as $item){
            \App\Category::create([
                'title' => $item
            ]);
        }
    }
}
