<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function postChangeCity(Request $request)
    {
        if($request->city == null){
            if(session()->get('city') != null)
                session()->forget('city');
            return response()->json('Все города', 200);
        }
        $city = City::find($request->city);
        session(['city' => $city->id]);
        return response()->json($city->title, 200);
    }
}
