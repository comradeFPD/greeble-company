<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/product.css">
    <link rel="stylesheet" href="/css/zoom.css">
    <title>Заказать дверь {{ $product->title }}</title>
</head>
<!-- Modal -->
<div class="modal fade" id="modalCall" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вы можете связаться с производителем</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='telmodal text-center'>
                    <p class='btn btn-success'>
                        <a href='tel:{{ $product->companies->phone }}'>
                            <i class="fa fa-phone" aria-hidden="true"></i> {{ $product->companies->phone }}
                        </a>
                    </p>
                    @if($product->companies->additional_phone != null)
                        <p class='btn btn-success'>
                            <a href='tel:{{ $product->companies->additional_phone }}'>
                                <i class="fa fa-phone" aria-hidden="true"></i> {{ $product->companies->additional_phone }}
                            </a>
                        </p>
                    @endif
                </div>
                <div class='telmodal_address text-center'>
                    <p class='text-center'>Адрес: <b>{{ $product->companies->address }}</b> </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='order_modal'>
                    <div id="contentdiv" class="contcustom">
                        <h2>Оставьте заявку на эту модель двери</h2>
                        <div>
                            <form action="#" id="request-form" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="material" value="{{ $product->material }}">
                                <input type="hidden" name="title" value="{{ $product->title }}">
                                <input type="hidden" name="type" value="{{ $product->getDoorType() }}">
                                <input type="hidden" name="price" value="{{ $product->price }}">
                                <input type="hidden" name="company_id" value="{{ $product->companies->id }}">
                                <input type="hidden" name="color-form"
                                       value="{{ str_replace(' ', '', $product->mainColor->title) }}">
                                <input type="hidden" name="gauge" value="0">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <input id="username" type="text" name="name" placeholder="Введите Ваше имя..." required>
                                <input id="phone" type="text" name="phone" placeholder="Ваш номер без 8">
                                <button id="button1" type="submit" class="btn btn-success btn-block btn-lg"
                                        style='padding:10px 0;font-size:16px;'>Отправить
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/podbor"><img src='/img/logo2.png' style='max-width:150px;'></a>
</nav>
<section id='product'>
    <div class='container'>
        <div class='row'>
            <div class='col-md-7 img' id='topimg'>
                @if($product->view_count >= 10)
                <p class='text-center eye_block'><i class="fa fa-eye" aria-hidden="true"></i><span> {{ $product->view_count }}</span></p>
                @endif
                <img src='{{ url('/storage/'.$product->photo) }}'
                     id="picture-{{ str_replace(' ', '', $product->mainColor->title) }}" data-action="zoom">
                @if($product->colors->count() > 0)
                    @foreach($product->colors as $color)
                        <img src="{{ url('/storage/'.$color->image) }}" alt=""
                             id="picture-{{ str_replace(' ', '', $color->color->title) }}" style="display: none" data-action="zoom">
                    @endforeach
                @endif
            </div>

            <div class='col-md-5'>
                <div class='div-right-block'>
                    <div class='logocomp'>
                        @if($product->companies->company_logo != null)
                            <img src='{{ url('/storage/'.$product->companies->company_logo) }}'>
                        @else
                            <h1 class="text-center h1_nologo"> {{ $product->companies->title }} </h1>
                        @endif
                        <div class='order-call btn-success' data-toggle="modal" data-target="#modalCall">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                    </div>
                    <h1>{{ $product->title }}</h1>
                    <div class='text-right-block'>
                        <p>{!! $product->companies->description  !!}</p>
                    </div>
                    <div class='hr'></div>
                    <div class='color-div'>
                        <div class='col-md-6 col-sm-12 materials_item'>
                            <p><i class="fa fa-paint-brush" aria-hidden="true"></i> Цвета: </p>
                        </div>
                        <ul class="chec-radio">
                            <li class="pz">
                                <label class="radio-inline">
                                    <input type="radio" checked
                                           id="{{ str_replace(' ', '', $product->mainColor->title) }}" name="color"
                                           class="pro-chx" value="{{ $product->mainColor->title }}">
                                    <div class="clab">{{ $product->mainColor->title }}</div>
                                </label>
                            </li>
                            @if($product->colors->count() > 0)
                                @foreach($product->colors as $color)
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="{{ str_replace(' ', '', $color->color->title) }}"
                                                   name="color" class="pro-chx" value="{{ $color->color->title }}">
                                            <div class="clab">{{ $color->color->title }}</div>
                                        </label>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class='hr'></div>
                    <div class='materials'>
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-6 col-sm-12 materials_item'>
                                    <p>Материал: <i class="fa fa-tree" aria-hidden="true"></i> {{ $product->material->title }}
                                    </p>
                                </div>
                                <div class='col-md-6 col-sm-12 materials_item'>
                                    <p>Тип: <i class="fa fa-window-restore"
                                               aria-hidden="true"></i> {{ $product->getDoorType() }} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='hr'></div>
                    <div class='materials'>
                        <div class='container'>
                            <div class='row'>
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">
                                        Нужен замерщик
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='hr'></div>
                    <div class='order'>
                        <p>
                            <span>от {{ $product->price }} <i class="fa fa-rub" aria-hidden="true"></i></span>
                            <a href='#' data-toggle="modal" data-target="#modalOrder" class='btn-lg btn-success'>
                                <i class="fa fa-phone" aria-hidden="true"></i>	Подать заявку
                            </a>
                        </p>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        * Полную цену комплекта, других параметров, уточняйте у производителя
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if($recommended->count() > 0)
    <section id='dop'>
        <div class='container'>
            <h2>Похожие варианты:</h2>
            <div class='row'>
                @foreach($recommended as $item)
                    <div class='col-md-3 dop_item'>
                        <img src='{{ url('/storage/'.$item->photo) }}'>
                        <p class='name_door text-center'>{{ $item->title }}</p>
                        <p class='price_door text-center'>{{ $item->price }} <i class="fa fa-rub" aria-hidden="true"></i>
                        </p>
                        <p>
                            <a href='{{ url('/product/'.$item->id) }}' class='btn btn-block btn-success'>Подробнее</a>
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
<!--Model Popup starts-->
<div class="container">
    <div class="row">
        <div class="modal fade" id="SuccessModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="thank-you-pop">
                            <p class='text-center'><i class="fa fa-check-circle" aria-hidden="true"></i></p>
                            <h1 class='text-center'>Спасибо!</h1>
                            <p class='text-center'>Ваша заявка отправлена, <br/>в ближайшее время с вами свяжутся!</p>
                            <p class='text-center'>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model Popup ends-->
<script src="{{ asset('/js/app.js') }}"></script>
<script src="/js/zoom.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="/js/jquery.inputmask.min.js"></script>
<script>
    $('input[name="color"]').click(function () {
        let id, idForForm;
        if ($(this).prop('checked') == true) {
            idForForm = $(this).val();

            id = 'picture-' + $(this).val().replace(' ', '');
            $('input[name="color-form"]').val(idForForm);
            console.log(id);
        }
        $('#topimg').find('img').each(function () {
            if ($(this).prop('id') == id) {
                $('#' + id).show();

            } else {
                $(this).hide();
            }
        })
    });


    $('#checkbox1').click(function () {
        if ($(this).prop('checked') == true) {
            $('input[name="gauge"]').val(1)
        } else {
            $('input[name="gauge"]').val(0)
        }
    })
    $(document).ready(function () {
        $('#phone').inputmask("+7-(999)-9999999");
        $('#request-form').submit(function (e) {
            e.preventDefault();
            if($('#phone').inputmask("isComplete")){
                $.post('{{ route('send-request') }}', $(this).serialize(), function () {
                    $('#SuccessModal').modal('show');
                    $('#modalOrder').modal('hide');
                })
            } else {
                alert('Заполните телефон правильно');
            }
        });
    })
</script>
</body>
</html>
