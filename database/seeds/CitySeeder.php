<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Майкоп',
            'Белореченск',
            'Лабинск',
            'Горячий ключ',
            'Армавир',
            'Ставрополь',
            'Краснодар',
            'Новороссийск',
            'Геленджик',
            'Джубга',
            'Туапсе',
            'Сочи',
            'Хадыженск',
            'Апшеронск',
            'Курганинск',
            'Усть-Лабинск',
            'Анапа',
            'Крымск',
            'Кореновск',
            'Тихорецк',
            'Кропоткин',
        ];
        foreach ($data as $item){
            \App\City::create([
                'title' => $item
            ]);
        }
    }
}
