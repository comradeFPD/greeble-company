<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoorColor extends Model
{
    protected $guarded = [];

    public function color()
    {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }
}
