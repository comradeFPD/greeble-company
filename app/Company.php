<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 *
 * @package App
 * @property int $id
 * @property string $title
 * @property string $phone
 * @property string|null $additional_phone
 * @property string $email
 * @property string $description
 * @property string|null $company_logo
 * @property-read Product[] products()
 */
class Company extends Model
{
    /**
     * Возвращает список всех продуктов компании
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'company_id', 'id');
    }
}
