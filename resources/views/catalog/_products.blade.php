@if($products->count() > 0)
    <div class="row">
        @foreach($products as $product)
            <div class='col-md-3 col-sm-6 col-xs-12'>
                <div class="product-container item-door">
                    <a href="{{ url('/product/'.$product->id) }}">
                        <img src="{{ url('/storage/'.$product->photo) }}" alt="" width="250" height="250">
                    </a>
                    <a href="{{ url('/product/'.$product->id) }}" target="_blank"><h6
                            class='title-door text-center'>{{ $product->title }}</h6></a>
                    <h5 class='title-price text-center'>Цена: {{ $product->price }} <i class="fa fa-rub"
                                                                                       aria-hidden="true"></i></h5>
                    <a href="{{ url('/product/'.$product->id) }}" class="btn btn-primary btn-block" target="_blank"
                       style='color:#fff;'>Подробнее</a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="text-center">
        {{ $products->links() }}
    </div>
@else
    <div class="card-body">
        <h4 class='text-center'>По вашему фильтру ничего не найдено, или же в данной категории нет продуктов</h4>
        <a href="#" class="btn btn-danger reset-filter" id="reset-filter">Сбросить фильтр</a>
    </div>
@endif
