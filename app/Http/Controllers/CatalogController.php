<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Color;
use App\Company;
use App\Mail\SendRequestForm;
use App\Material;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CatalogController extends Controller
{
    public function getViewAll($id)
    {
        $cities = City::all();
        $products = Product::where('category_id', $id)->join('companies', 'products.company_id', '=', 'companies.id')
            ->select('products.*')->orderBy('id', 'desc');

        if(session()->get('city') != null){
            $products->where('companies.city_id', session()->get('city'));
        }
        $materials = Material::whereIn('id', $products->get()->pluck('material_id'))->get();
        $colors = Color::where(function ($query) use($products){
           $query->whereIn('id', $products->get()->pluck('color_id'));
           foreach ($products as $product){
               $query->orWhereIn('id', $product->additionalColors->pluck('color_id'));
           }
        })->get();
        $category = Category::find($id);
        $products = $products->paginate(9);
        return response()->view('catalog.new-main', [
            'products' => $products,
            'cities' => $cities,
            'colors' => $colors,
            'category' => $category,
            'materials' => $materials
        ]);
    }

    public function postFilter(Request $request)
    {
        $data = $request->except('_token', 'colors');
        $city = session()->get('city');
        $query = Product::whereBetween('price', [$data['price_from'], $data['price_to']])
            ->join('companies', 'products.company_id', '=', 'companies.id')
        ->select('products.*');
        $query = Product::createQuery($query, $request->colors, 'colors');
        if($city != null){
            $query->where('companies.city_id', $city);
        }
        foreach ($data as $key => $item){
            if($key == 'price_from' || $key == 'price_to' || $key == 'price')
                continue;
            if($item == null)
                continue;
            $query = Product::createQuery($query, $item, $key);
        }
        $products = $query->orderBy('id', 'desc')->paginate(9);
        return \Illuminate\Support\Facades\View::make('catalog._products', ['products' => $products])->render();
    }

    public function getViewProduct($id)
    {
        $product = Product::find($id);
        $recommended = Product::where([
            ['is_glass', $product->is_glass],
            ['is_blind', $product->is_blind],
            ['is_with_glass', $product->is_with_glass],
            ['id', '!=', $product->id]
        ])->get();
        $product->view_count++;
        $product->save();
        $recommended = $recommended->filter(function (Product $item) use ($product){
            return $item->companies->city_id == $product->companies->city_id ? $item : '';
        });
        return response()->view('catalog.product', [
            'product' => $product,
            'recommended' => $recommended
        ]);
    }

    public function postSendRequestToCompany(Request $request)
    {
        $data = $request->except('_token');
        Mail::to(Company::find($data['company_id'])->email)->send(new SendRequestForm($data));
    }

    public function getViewCategories()
    {
        $categories = Category::all();
        return response()->view('catalog.categories', [
            'categories' => $categories
        ]);
    }
}
