<?php

namespace App\Mail;

use App\Color;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SendRequestForm extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @param array $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $color = Color::where('title', $this->data['color-form'])->first();
        if ($color->doorColors()->where('product_id', $this->data['product_id'])->first() != null) {
            $this->data['img_url'] = $color->doorColors()->where('product_id', $this->data['product_id'])->first(
            )->image;
        }
        else {
            $this->data['img_url'] = Product::find($this->data['product_id'])->photo;
        }

        return $this->subject('Новый заказ!')->markdown(
            'mail.request-form',
            [
                'data' => $this->data
            ]
        );
    }
}
