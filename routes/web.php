<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\City;
use App\Color;
use App\Material;
use App\Product;

Route::get('/', function () {
    if(session()->get('city') == null){
        $prodcuts = Product::all()->shuffle()->slice(0, 5);
    } else {
        $prodcuts = Product::select('products.*')->join('companies', 'companies.id', '=', 'products.company_id')
            ->where('companies.city_id', session()->get('city'))->get()->shuffle()->slice(0, 5);
    }
    return view('welcome', [
        'cities' => \App\City::orderBy('title', 'asc')->get(),
        'products' => $prodcuts
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/delete-color/{id}', 'VoyagerProductsController@getDeleteColor');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/podbor', 'CatalogController@getViewCategories');
Route::get('/products/{id}', 'CatalogController@getViewAll');
Route::get('/product/{id}', 'CatalogController@getViewProduct');
Route::post('/send-request', 'CatalogController@postSendRequestToCompany')->name('send-request');
Route::post('/select-city', 'HomeController@postChangeCity')->name('select-city');
Route::post('/filter', 'CatalogController@postFilter')->name('filter');
Route::get('/test', function (){
    $id = 1;
    $cities = City::all();
    $products = Product::where('category_id', $id)->join('companies', 'products.company_id', '=', 'companies.id')
        ->select('products.*')->orderBy('id', 'desc');

    if(session()->get('city') != null){
        $products->where('companies.city_id', session()->get('city'));
    }
    $materials = Material::whereIn('id', $products->get()->pluck('material_id'))->get();
    $colors = Color::where(function ($query) use($products){
        $query->whereIn('id', $products->get()->pluck('color_id'));
        foreach ($products as $product){
            $query->orWhereIn('id', $product->additionalColors->pluck('color_id'));
        }
    })->get();
    $category = Category::find($id);
    $products = $products->paginate(9);
    return response()->view('catalog.main', [
        'products' => $products,
        'cities' => $cities,
        'colors' => $colors,
        'category' => $category,
        'materials' => $materials
    ]);
});
