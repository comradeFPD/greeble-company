@component('mail::message')
У вас новый заказ! Детали:<br/>
Название двери: {{ $data['title'] }}<br>
Материал двери: {{ $data['material'] }}<br>
Тип двери: {{ $data['type'] }}<br>
Имя заказчика: {{ $data['name'] }}<br>
Телефон: {{ $data['phone'] }}<br>
Цена: {{ $data['price'] }} руб.<br>
Нужен замерщик? :  {{ $data['gauge'] == 1 ? 'Дв' : 'Нет' }}<br>
Фото товара:<br>
<img src="{{ asset('/storage/'.$data['img_url']) }}" alt="" srcset="">
    @endcomponent
