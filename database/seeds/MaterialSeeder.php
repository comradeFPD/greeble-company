<?php

use Illuminate\Database\Seeder;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $str = 'Массив сосны,Массив ели,Массив ольхи,Массив бука,Массив  ясени,Массив дуба,Массив клена,Массив вишни,Массив березы,Массив лиственницы,Экошпон,Шпон,МДФ под покраску,МДФ окрашенные,Ламинированные МДФ,С покрытием ПВХ';
        $arr = explode(',', $str);
        foreach ($arr as $item){
            \App\Material::create([
                'title' => $item
            ]);
        }
    }
}
