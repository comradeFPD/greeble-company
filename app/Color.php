<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public function doorColors()
    {
        return $this->hasMany(DoorColor::class, 'color_id', 'id');
    }
}
