@extends('layouts.filter')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="/css/catalog/filter.css">
<link rel="stylesheet" href="/css/ion.rangeSlider.min.css">
@section('content')
    <div class="page-wrapper default-theme sidebar-bg bg1 toggled" id="sidebar">
        <nav id="sidebar" class="sidebar-wrapper">
            <form action="#" method="post" id="filter-form" enctype="multipart/form-data">
                <div class="sidebar-content">
                    <!-- sidebar-brand  -->
                    <div class="sidebar-item sidebar-brand">
                        <p><a href='/'><img src='/img/logo.png'></a></p>
                        <p class='text-center'>
                            <a href="#" id="city-button" class='btn btn-secondary btn-sm'>
                                Ваш город?
                            </a>
                        </p>
                    </div>
                    <!-- sidebar-header  -->
                    <h6>Выберите параметры и нажмите кнопку «Подобрать варианты»</h6>
                @csrf

                <!-- sidebar-menu  -->
                    <div class="sidebar-item sidebar-menu">

                    </div>
                    <div class=" sidebar-item sidebar-menu">
                        <ul>
                            <li class="header-menu">
                                <div class="form-group">
                                    <h5>Выберите желаемый цвет</h5>
                                    <div class='colors'>
                                        @foreach($colors as $color)
                                            <p class='text-left'>
                                                <label class="check" for="color-{{ $color->id }}">{{ $color->title }}
                                                    <input type="checkbox" name="colors[]" id="color-{{ $color->id }}" value="{{ $color->id }}">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </p>
                                        @endforeach
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label for="material">
                                        <h5>Все материалы </h5>
                                        <select name="material_id" id="material" class="custom-select">
                                            <option value="">Все</option>
                                            @foreach($materials as $material)
                                                <option value="{{ $material->id }}">{{ $material->title }}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label for="door-param">
                                        <h5>Вид дверей</h5>
                                        <select name="door_param" id="door-param" class="custom-select">
                                            <option value="">Все</option>
                                            <option value="is_blind">Глухие</option>
                                            <option value="is_with_glass">Со стеклом</option>
                                        </select>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label for="price-policy">
                                        <h5>Выберите класс дверей </h5>
                                        <select id="price-policy" class="custom-select">
                                            <option value="">Все</option>
                                            <option value="2">Премиум</option>
                                            <option value="1">Элитные</option>
                                            <option value="3">Бюджетные</option>
                                        </select>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class='filter_price'>
                                    <input type="hidden" name="category_id" value="{{ $category->id }}">
                                    <h5><label for="slider">Фильтр цены</label></h5>
                                    <input type="text" id="slider" name="price" class="form-control">
                                    <div class="form-group" style='margin-top:10px;'>
                                        <label for="from" style='width:50%;display:inline!important;'>
                                            <input type="text" name="price_from" id="from" class="form-control" style='width:48%;display:inline;'>
                                            <input type="text" name="price_to" id="to" class="form-control" style='width:48%;display:inline;'>
                                        </label>
                                        <span class='text-center' style='display:block;'>от - до</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <button type="submit" class="btn btn-primary btn-block btn-sm btn-danger reset-filter">Сбросить
                                    фильтр
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!-- sidebar-menu  -->
                </div>
                <!-- sidebar-footer  -->
                <div class="sidebar-footer">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" id="send-filter"><i class="fa fa-search" aria-hidden="true"></i> Подобрать варианты</button>
                </div>
            </form>
        </nav>
        <!-- page-content  -->
        <main class="page-content pt-2">
            <div id="overlay" class="overlay"></div>
            <a id="toggle-sidebar" class="btn btn-secondary rounded-0 text-center" href="#" style='position:fixed;top:0;'>
                <span><i class="fa fa-bars" aria-hidden="true"></i></span>
            </a>
            <div class="container-fluid p-5">
                <div id="products">
                    @include('catalog._products', [
        'products' => $products
    ])
                </div>
            </div>
        </main>
        <!-- page-content" -->
    </div>
    <a class="up" href="#" style="position: fixed; right: 10px; bottom: 10px"><img src="/img/up.png" alt="icon"></a>
    <!-- page-wrapper -->
@endsection

@push('scripts')
    <!-- using online scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
    </script>
    <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

    <script src="/js/catalog/filter.js"></script>
    <script src="/js/ion.rangeSlider.min.js"></script>
    <script>
        $('document').ready(function () {
            if(window.innerWidth < 1000){
                $('#sidebar').removeClass('toggled')
            }
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('.up').fadeIn();
                } else {
                    $('.up').fadeOut();
                }
            });
            $('.up').click(function(e) {
                e.preventDefault();
                $('body,html').animate({scrollTop:0},700);
            })
            let btn = null;
            var $range = $("#slider"),
                $inputFrom = $("#from"),
                $inputTo = $("#to"),
                instance,
                min = 0,
                max = 50000,
                from = 0,
                to = 0;

            $range.ionRangeSlider({
                skin: "round",
                type: "double",
                min: min,
                max: max,
                from: min,
                to: max,
                onStart: updateInputs,
                onChange: updateInputs,
            });
            instance = $range.data("ionRangeSlider");

            function updateInputs(data) {
                from = data.from;
                to = data.to;

                $inputFrom.prop("value", from);
                $inputTo.prop("value", to);
            }

            $inputFrom.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < min) {
                    val = min;
                } else if (val > to) {
                    val = to;
                }

                instance.update({
                    from: val
                });
            });

            $inputTo.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < from) {
                    val = from;
                } else if (val > max) {
                    val = max;
                }

                instance.update({
                    to: val
                });
            });
            $('#price-policy').change(function () {
                let val = parseInt($(this).val());
                switch (val) {
                    case 1: {
                        $inputTo.val(25000);
                        $inputFrom.val(15000);
                        instance.update({
                            from: 15000,
                            to: 25000
                        })
                        break;
                    }
                        ;
                    case 2: {
                        $inputTo.val(max);
                        $inputFrom.val(25000);
                        instance.update({
                            from: 25000,
                            to: max
                        })
                        break;
                    }
                    case 3: {
                        $inputTo.val(15000);
                        $inputFrom.val(min);
                        instance.update({
                            from: min,
                            to: 15000
                        })
                        break;
                    }
                    default: {
                        $inputTo.val(0);
                        $inputFrom.val(50000);
                        instance.update({
                            from: 0,
                            to: 50000
                        })
                        break;
                    }
                }
            })
            $('.reset-filter').click(function (e) {
                e.preventDefault();
                resetFilter();
                $('#filter-form').submit();
            });



            function resetFilter() {
                $inputFrom.val(min);
                $inputTo.val(max);
                instance.update({
                    from: min,
                    to: max
                });
                $('input[type="checkbox"]').prop('checked', false)
                $('select').val('');
            }


            $('#filter-form').submit(function (e) {
                e.preventDefault();
                $('#send-filter').prop('disabled', 'disabled');
                $.post('{{ route('filter') }}', $(this).serialize(), function (data) {
                    $('#products').html(data);
                    $('#send-filter').prop('disabled', false);
                    $('body').animate({scrollTop: 0}, 500);
                    btn = document.getElementById('reset-filter');
                    if(btn != null){
                        btn.onclick = function () {
                            resetFilter();
                            $('#filter-form').submit();
                        }
                    }
                })
            })
        });




    </script>
@endpush

