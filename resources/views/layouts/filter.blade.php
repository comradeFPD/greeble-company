<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
@php
    $cities = \App\City::orderBy('title', 'asc')->get();
@endphp
<body>
<div id="app">

    <main class="py-4">
        @yield('content')
    </main>
</div>
<!-- City modal start !-->
<div class="modal fade" id="selectCity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='order_modal'>
                    <div id="contentdiv" class="contcustom">
                        <h2>Выберите город</h2>
                        <div>
                            <form action="#" id="city-form" method="post" enctype="multipart/form-data">
                                @csrf
                                <select name="city" id="" class="custom-select">
                                    <option value="">Все города</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->title }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <br>
                                <button id="button1" type="submit" class="btn btn-success btn-block btn-lg" style='padding:10px 0;font-size:16px;'>Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- City modal end !-->
<script src="/js/selectCity.js"></script>
@stack('scripts')
</body>
</html>
