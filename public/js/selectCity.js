$('#city-button').click(function (e) {
    e.preventDefault();
    $('#selectCity').modal('show');
});
$('#city-form').submit(function (e) {
    e.preventDefault();
    $.post('/select-city', $(this).serialize(), function (data) {
        $('#city-button').text(data+' (изменить)');
        $('#selectCity').modal('hide');
        document.location.reload();
    })
});
