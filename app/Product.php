<?php

namespace App;

use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App
 *
 * @property int          $id
 * @property string       $photo
 * @property string       $additional_photos
 * @property int          $price
 * @property string       $description
 * @property string       $color
 * @property int          $company_id
 * @property boolean      $is_blind
 * @property string       $material
 * @property boolean      $is_glass
 * @property boolean      $is_with_glass
 * @property string       $price_policy
 * @property string       $additional
 * @property-read Company companies()
 */
class Product extends Model
{
    /**
     * Получение компании которой принадлежит продукт
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public static function createQuery(Builder $query, $data, $propertyName, $method = 'where')
    {
        if ($data == null) {
            return $query;
        }
        if ($propertyName == 'door_param') {
            $query->$method($data, true);

            return $query;
        }
        if ($propertyName == 'colors') {
            $query->leftJoin('door_colors', 'products.id', '=', 'door_colors.product_id');

                $query->where(
                    function ($query) use ($data) {
                        $query->whereIn('products.color_id', $data);
                        $query->orWhereIn('door_colors.color_id', $data);
            });
            return $query;
        }
        $query->$method($propertyName, $data);

        return $query;
    }

    /**
     *    Собираем строку с классами для фильтра
     *
     * @return string
     */
    public function getClassesForFilter()
    {
        $classes = 'mix ';
        $classes .= ' ' . $this->mainColor->title . ' ';
        $classes .= ' ' . $this->material . ' ';
        $classes .= $this->is_blind ? 'blind ' : ' ';
        $classes .= $this->is_with_glass ? 'with_glass ' : ' ';
        $classes .= explode(' ', $this->price_policy)[0] . ' ';
        $classes .= $this->additional . ' ';

        return $classes;
    }

    /**
     * Получаем тип двери и возвращаем перевод
     *
     * @return mixed
     */
    public function getDoorType()
    {
        $translate = [
            'is_blind'      => 'Глухая',
            'is_glass'      => 'Стеклянная',
            'is_with_glass' => 'С остеклением'
        ];
        if ($this->is_blind) {
            $doorType = 'is_blind';
        }
        elseif ($this->is_glass) {
            $doorType = 'is_glass';
        }
        else {
            $doorType = 'is_with_glass';
        }

        return $translate[$doorType];
    }

    public function colors()
    {
        return $this->hasMany(DoorColor::class, 'product_id', 'id');
    }

    public function mainColor()
    {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id', 'id');
    }
}
